<?php

namespace Source\Core;

/**
 * Class Session
 * @package Source\Core
 */
class Session
{
    /** é uma classe state less ou seja q ñ possui atributos e portanto ñ guarda o estado do objeto
     * Session constructor.
     */
    // serve p/ q ele cria sessão ao instanciar a classe Session
    public function __construct()
    {
        if (!session_id()) { // testa se a sessão já existe
            session_save_path(CONF_SES_PATH); // define onde serão salvas as sessões
            session_start();
        }
    }

    /**
     * @param $name
     * @return null|mixed
     */
    public function __get($name)
    {
        if (!empty($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        return null;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->has($name);
    }

    /**
     * @return null|object
     */
    // converte o array super global SESSION num objeto
    public function all(): ?object
    {
        return (object)$_SESSION;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return Session
     */
    // pode receber tanto um valor primitivo qnt um array c/ valor
    public function set(string $key, $value): Session
    {
        $_SESSION[$key] = (is_array($value) ? (object)$value : $value);
        return $this;
    }

    /**
     * @param string $key
     * @return Session
     */
    public function unset(string $key): Session
    {
        unset($_SESSION[$key]);
        return $this;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return isset($_SESSION[$key]);
    }

    /**
     * @return Session
     */
    // é importante em algumas situações renova o id do usuário, eliminando seu arquivo de sessao juntamente c/ o id
    // porém mantendo os dados do usuário e recebdno um novo id 
    public function regenerate(): Session
    {
        session_regenerate_id(true);
        return $this;
    }

    /**
     * @return Session
     */
    // finaliza sessão, faz logoff
    public function destroy(): Session
    {
        session_destroy();
        return $this;
    }

    /**
     * @return null|Message
     */
    public function flash(): ?Message
    {
        if ($this->has("flash")) {
            $flash = $this->flash;
            $this->unset("flash");
            return $flash;
        }
        return null;
    }

    /**
     * CSRF Token
     */
    public function csrf(): void
    {
        $_SESSION['csrf_token'] = base64_encode(random_bytes(20));
    }
}